import React from 'react';
import './App.scss';
import Countdown from './components/Countdown';
import DigitalClock from './components/DigitalClock';
import Stopwatch from './components/Stopwatch';

function App() {
  return (
    <div className='container'>
    <DigitalClock></DigitalClock>
    <Countdown></Countdown>
    <Stopwatch></Stopwatch>
    </div>
  );
}

export default App;
